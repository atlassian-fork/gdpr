-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_environment where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

