-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,task_header as task_header_before,replace(task_header, '<OLD_VALUE>', '<NEW_VALUE>') as task_header_after from audit_log where task_header like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set task_header = replace(task_header, '<OLD_VALUE>', '<NEW_VALUE>') where task_header like '%<OLD_VALUE>%' ;

