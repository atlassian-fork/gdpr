-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,docker_pipeline_config as docker_pipeline_config_before,replace(docker_pipeline_config, '<OLD_VALUE>', '<NEW_VALUE>') as docker_pipeline_config_after from deployment_env_config where docker_pipeline_config like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_env_config set docker_pipeline_config = replace(docker_pipeline_config, '<OLD_VALUE>', '<NEW_VALUE>') where docker_pipeline_config like '%<OLD_VALUE>%' ;

