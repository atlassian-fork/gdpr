-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,lower_group_name as lower_group_name_before,'<NEW_VALUE>' as lower_group_name_after from cwd_group where lower_group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_group set lower_group_name = '<NEW_VALUE>' where lower_group_name = '<OLD_VALUE>' ;

