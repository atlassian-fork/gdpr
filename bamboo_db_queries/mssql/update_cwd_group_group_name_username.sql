-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_group where group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_group set group_name = '<NEW_VALUE>' where group_name = '<OLD_VALUE>' ;

