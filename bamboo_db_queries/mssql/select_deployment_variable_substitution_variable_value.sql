-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mssql

select deployment_variable_substitution.* from deployment_variable_substitution where variable_value like '%<OLD_VALUE>%' ;

