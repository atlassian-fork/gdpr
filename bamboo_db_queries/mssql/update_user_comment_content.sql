-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select user_comment.*,content as content_before,replace(content, '<OLD_VALUE>', '<NEW_VALUE>') as content_after from user_comment where content like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update user_comment set content = replace(content, '<OLD_VALUE>', '<NEW_VALUE>') where content like '%<OLD_VALUE>%' ;

