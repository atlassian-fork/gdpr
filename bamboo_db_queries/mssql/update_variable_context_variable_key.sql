-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select variable_context.*,variable_key as variable_key_before,replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') as variable_key_after from variable_context where variable_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update variable_context set variable_key = replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') where variable_key like '%<OLD_VALUE>%' ;

