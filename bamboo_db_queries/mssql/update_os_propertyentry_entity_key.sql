-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_key as entity_key_before,replace(entity_key, '<OLD_VALUE>', '<NEW_VALUE>') as entity_key_after from os_propertyentry where entity_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update os_propertyentry set entity_key = replace(entity_key, '<OLD_VALUE>', '<NEW_VALUE>') where entity_key like '%<OLD_VALUE>%' ;

