-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select queue.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from queue where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update queue set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;

