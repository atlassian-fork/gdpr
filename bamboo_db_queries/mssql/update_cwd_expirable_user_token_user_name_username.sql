-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_expirable_user_token.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from cwd_expirable_user_token where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_expirable_user_token set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

