-- Type        : update
-- Origin      : bamboo
-- Description : Default group name
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_app_dir_default_groups.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_app_dir_default_groups where group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_app_dir_default_groups set group_name = '<NEW_VALUE>' where group_name = '<OLD_VALUE>' ;

