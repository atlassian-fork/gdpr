-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_project_item.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_project_item where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_project_item set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

