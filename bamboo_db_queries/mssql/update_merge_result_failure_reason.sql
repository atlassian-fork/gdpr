-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select merge_result.*,failure_reason as failure_reason_before,replace(failure_reason, '<OLD_VALUE>', '<NEW_VALUE>') as failure_reason_after from merge_result where failure_reason like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update merge_result set failure_reason = replace(failure_reason, '<OLD_VALUE>', '<NEW_VALUE>') where failure_reason like '%<OLD_VALUE>%' ;

