-- Type        : update
-- Origin      : bamboo
-- Description : user permissions - ownership
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select acl_object_identity.*,owner_sid as owner_sid_before,'<NEW_VALUE>' as owner_sid_after from acl_object_identity where (owner_sid = '<OLD_VALUE>' ) AND owner_type  = 'PRINCIPAL';

-- + UPDATE (be careful)
update acl_object_identity set owner_sid = '<NEW_VALUE>' where (owner_sid = '<OLD_VALUE>' ) AND owner_type  = 'PRINCIPAL';

