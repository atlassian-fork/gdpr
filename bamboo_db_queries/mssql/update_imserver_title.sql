-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,title as title_before,replace(title, '<OLD_VALUE>', '<NEW_VALUE>') as title_after from imserver where title like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update imserver set title = replace(title, '<OLD_VALUE>', '<NEW_VALUE>') where title like '%<OLD_VALUE>%' ;

