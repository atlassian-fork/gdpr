-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_name as entity_name_before,replace(entity_name, '<OLD_VALUE>', '<NEW_VALUE>') as entity_name_after from os_propertyentry where entity_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update os_propertyentry set entity_name = replace(entity_name, '<OLD_VALUE>', '<NEW_VALUE>') where entity_name like '%<OLD_VALUE>%' ;

