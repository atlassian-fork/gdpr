-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_variable_substitution.*,variable_value as variable_value_before,replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') as variable_value_after from deployment_variable_substitution where variable_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_variable_substitution set variable_value = replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') where variable_value like '%<OLD_VALUE>%' ;

