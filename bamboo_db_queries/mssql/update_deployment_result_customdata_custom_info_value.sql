-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_result_customdata.*,custom_info_value as custom_info_value_before,replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') as custom_info_value_after from deployment_result_customdata where custom_info_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_result_customdata set custom_info_value = replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') where custom_info_value like '%<OLD_VALUE>%' ;

