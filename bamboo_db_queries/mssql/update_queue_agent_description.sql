-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select queue.*,agent_description as agent_description_before,replace(agent_description, '<OLD_VALUE>', '<NEW_VALUE>') as agent_description_after from queue where agent_description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update queue set agent_description = replace(agent_description, '<OLD_VALUE>', '<NEW_VALUE>') where agent_description like '%<OLD_VALUE>%' ;

