-- Type        : update
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from chain_stage where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

