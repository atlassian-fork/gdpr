-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select quick_filters.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from quick_filters where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update quick_filters set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

