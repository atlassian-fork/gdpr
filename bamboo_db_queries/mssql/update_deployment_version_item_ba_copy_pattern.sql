-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,copy_pattern as copy_pattern_before,replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') as copy_pattern_after from deployment_version_item_ba where copy_pattern like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_item_ba set copy_pattern = replace(copy_pattern, '<OLD_VALUE>', '<NEW_VALUE>') where copy_pattern like '%<OLD_VALUE>%' ;

