-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,alias_name as alias_name_before,replace(alias_name, '<OLD_VALUE>', '<NEW_VALUE>') as alias_name_after from cwd_application_alias where alias_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_application_alias set alias_name = replace(alias_name, '<OLD_VALUE>', '<NEW_VALUE>') where alias_name like '%<OLD_VALUE>%' ;

