-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,display_name as display_name_before,'<NEW_VALUE>' as display_name_after from cwd_user where display_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set display_name = '<NEW_VALUE>' where display_name = '<OLD_VALUE>' ;

