-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,triggers_xml_data as triggers_xml_data_before,replace(triggers_xml_data, '<OLD_VALUE>', '<NEW_VALUE>') as triggers_xml_data_after from deployment_environment where triggers_xml_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_environment set triggers_xml_data = replace(triggers_xml_data, '<OLD_VALUE>', '<NEW_VALUE>') where triggers_xml_data like '%<OLD_VALUE>%' ;

