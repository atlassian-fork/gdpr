-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,last_name as last_name_before,'<NEW_VALUE>' as last_name_after from cwd_user where last_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set last_name = '<NEW_VALUE>' where last_name = '<OLD_VALUE>' ;

