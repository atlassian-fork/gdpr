-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,string_val as string_val_before,replace(string_val, '<OLD_VALUE>', '<NEW_VALUE>') as string_val_after from os_propertyentry where string_val like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update os_propertyentry set string_val = replace(string_val, '<OLD_VALUE>', '<NEW_VALUE>') where string_val like '%<OLD_VALUE>%' ;

