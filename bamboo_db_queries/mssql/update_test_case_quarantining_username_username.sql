-- Type        : update
-- Origin      : bamboo
-- Description : user that has quarantined test case
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select test_case.*,quarantining_username as quarantining_username_before,'<NEW_VALUE>' as quarantining_username_after from test_case where quarantining_username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update test_case set quarantining_username = '<NEW_VALUE>' where quarantining_username = '<OLD_VALUE>' ;

