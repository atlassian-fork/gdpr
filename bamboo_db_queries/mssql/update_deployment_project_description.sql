-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_project.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from deployment_project where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_project set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

