-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

select deployment_version_item_ba.* from deployment_version_item_ba where copy_pattern like '%<OLD_VALUE>%' ;

