-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,msg as msg_before,replace(msg, '<OLD_VALUE>', '<NEW_VALUE>') as msg_after from audit_log where msg like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set msg = replace(msg, '<OLD_VALUE>', '<NEW_VALUE>') where msg like '%<OLD_VALUE>%' ;

