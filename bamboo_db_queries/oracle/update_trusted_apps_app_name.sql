-- Type        : update
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select trusted_apps.*,app_name as app_name_before,REGEXP_REPLACE(app_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as app_name_after from trusted_apps where REGEXP_LIKE (LOWER(app_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update trusted_apps set app_name = REGEXP_REPLACE(app_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(app_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

