-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select imserver.*,host as host_before,REGEXP_REPLACE(host,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as host_after from imserver where REGEXP_LIKE (LOWER(host),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update imserver set host = REGEXP_REPLACE(host,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(host),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

