-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : oracle

select notifications.* from notifications where (LOWER(recipient) = LOWER('<OLD_VALUE>') ) AND recipient_type  = 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

