-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : oracle

select merge_result.* from merge_result where REGEXP_LIKE (LOWER(failure_reason),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

