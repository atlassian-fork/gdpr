-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select AO_C7F71E_OAUTH_SVC_PROV_TKNS.*,VALUE as VALUE_before,REGEXP_REPLACE(VALUE,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as VALUE_after from AO_C7F71E_OAUTH_SVC_PROV_TKNS where REGEXP_LIKE (LOWER(VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update AO_C7F71E_OAUTH_SVC_PROV_TKNS set VALUE = REGEXP_REPLACE(VALUE,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

