-- Type        : update
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select artifact.*,label as label_before,REGEXP_REPLACE(label,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as label_after from artifact where REGEXP_LIKE (LOWER(label),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update artifact set label = REGEXP_REPLACE(label,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(label),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

