-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : oracle

select deployment_version_item_ba.* from deployment_version_item_ba where REGEXP_LIKE (LOWER(label),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

