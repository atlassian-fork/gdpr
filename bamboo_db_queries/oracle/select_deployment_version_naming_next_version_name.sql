-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : oracle

select deployment_version_naming.* from deployment_version_naming where REGEXP_LIKE (LOWER(next_version_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

