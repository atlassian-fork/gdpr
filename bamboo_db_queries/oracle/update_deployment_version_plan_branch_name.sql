-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,plan_branch_name as plan_branch_name_before,REGEXP_REPLACE(plan_branch_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as plan_branch_name_after from deployment_version where REGEXP_LIKE (LOWER(plan_branch_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update deployment_version set plan_branch_name = REGEXP_REPLACE(plan_branch_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(plan_branch_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

