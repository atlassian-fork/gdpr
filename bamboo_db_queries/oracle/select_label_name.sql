-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : oracle

select label.* from label where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

