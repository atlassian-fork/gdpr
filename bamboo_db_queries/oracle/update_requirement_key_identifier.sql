-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select requirement.*,key_identifier as key_identifier_before,REGEXP_REPLACE(key_identifier,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as key_identifier_after from requirement where REGEXP_LIKE (LOWER(key_identifier),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update requirement set key_identifier = REGEXP_REPLACE(key_identifier,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(key_identifier),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

