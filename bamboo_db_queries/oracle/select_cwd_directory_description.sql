-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : oracle

select cwd_directory.* from cwd_directory where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

