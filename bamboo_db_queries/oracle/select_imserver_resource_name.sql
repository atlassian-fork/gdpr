-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

select imserver.* from imserver where REGEXP_LIKE (LOWER(resource_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

