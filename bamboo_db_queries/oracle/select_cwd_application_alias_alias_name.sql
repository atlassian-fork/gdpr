-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : oracle

select cwd_application_alias.* from cwd_application_alias where REGEXP_LIKE (LOWER(alias_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

