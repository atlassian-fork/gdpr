-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select queue.* from queue where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

