-- Type        : select
-- Origin      : bamboo
-- Description : Test error content
-- Database    : oracle

select test_error.* from test_error where REGEXP_LIKE (LOWER(error_content),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

