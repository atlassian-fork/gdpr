-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : oracle

select deployment_version.* from deployment_version where REGEXP_LIKE (LOWER(plan_branch_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

