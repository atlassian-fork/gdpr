-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : oracle

select user_comment.* from user_comment where REGEXP_LIKE (LOWER(content),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

