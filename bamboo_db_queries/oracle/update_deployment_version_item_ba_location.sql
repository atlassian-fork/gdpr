-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,location as location_before,REGEXP_REPLACE(location,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as location_after from deployment_version_item_ba where REGEXP_LIKE (LOWER(location),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update deployment_version_item_ba set location = REGEXP_REPLACE(location,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(location),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

