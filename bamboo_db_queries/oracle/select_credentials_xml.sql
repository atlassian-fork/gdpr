-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : oracle

select credentials.* from credentials where REGEXP_LIKE (LOWER(xml),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

