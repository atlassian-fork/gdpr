-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : oracle

select deployment_env_config.* from deployment_env_config where REGEXP_LIKE (LOWER(plugin_config),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

