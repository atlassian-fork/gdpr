-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : oracle

select project.* from project where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

