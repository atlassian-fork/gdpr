-- Type        : update
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "trusted_apps".*,app_name as app_name_before,REGEXP_REPLACE(app_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as app_name_after from trusted_apps where LOWER(app_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update trusted_apps set app_name = REGEXP_REPLACE(app_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(app_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

