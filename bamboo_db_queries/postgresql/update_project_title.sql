-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "project".*,title as title_before,REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as title_after from project where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update project set title = REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

