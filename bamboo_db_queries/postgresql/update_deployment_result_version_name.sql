-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_result".*,version_name as version_name_before,REGEXP_REPLACE(version_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as version_name_after from deployment_result where LOWER(version_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_result set version_name = REGEXP_REPLACE(version_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(version_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

