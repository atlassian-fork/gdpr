-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : postgresql

select "requirement".* from requirement where LOWER(key_identifier) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

