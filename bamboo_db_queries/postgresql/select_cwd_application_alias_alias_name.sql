-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

select "cwd_application_alias".* from cwd_application_alias where LOWER(alias_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

