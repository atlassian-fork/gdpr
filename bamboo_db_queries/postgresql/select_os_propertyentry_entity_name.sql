-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

select "os_propertyentry".* from os_propertyentry where LOWER(entity_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

