-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_group".*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_group where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_group set group_name = '<NEW_VALUE>' where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

