-- Type        : select
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : postgresql

select "user_commit".* from user_commit where LOWER(commit_comment_clob) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

