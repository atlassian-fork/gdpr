-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "queue".*,title as title_before,REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as title_after from queue where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update queue set title = REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

