-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_version_item_ba".*,copy_pattern as copy_pattern_before,REGEXP_REPLACE(copy_pattern,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as copy_pattern_after from deployment_version_item_ba where LOWER(copy_pattern) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_version_item_ba set copy_pattern = REGEXP_REPLACE(copy_pattern,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(copy_pattern) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

