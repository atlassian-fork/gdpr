-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : postgresql

select "plan_vcs_history".* from plan_vcs_history where LOWER(xml_custom_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

