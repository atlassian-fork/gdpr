-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "artifact_definition".*,copy_pattern as copy_pattern_before,REGEXP_REPLACE(copy_pattern,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as copy_pattern_after from artifact_definition where LOWER(copy_pattern) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update artifact_definition set copy_pattern = REGEXP_REPLACE(copy_pattern,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(copy_pattern) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

