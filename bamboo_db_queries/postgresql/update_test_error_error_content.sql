-- Type        : update
-- Origin      : bamboo
-- Description : Test error content
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "test_error".*,error_content as error_content_before,REGEXP_REPLACE(error_content,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as error_content_after from test_error where LOWER(error_content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update test_error set error_content = REGEXP_REPLACE(error_content,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(error_content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

