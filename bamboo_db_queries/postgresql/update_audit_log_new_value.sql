-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "audit_log".*,new_value as new_value_before,REGEXP_REPLACE(new_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as new_value_after from audit_log where LOWER(new_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update audit_log set new_value = REGEXP_REPLACE(new_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(new_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

