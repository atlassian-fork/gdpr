-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : postgresql

select "cwd_application".* from cwd_application where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

