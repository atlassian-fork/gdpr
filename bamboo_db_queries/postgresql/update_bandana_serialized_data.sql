-- Type        : update
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "bandana".*,serialized_data as serialized_data_before,REGEXP_REPLACE(serialized_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as serialized_data_after from bandana where LOWER(serialized_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update bandana set serialized_data = REGEXP_REPLACE(serialized_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(serialized_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

