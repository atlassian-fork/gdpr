-- Type        : select
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : postgresql

select "trusted_apps".* from trusted_apps where LOWER(app_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

