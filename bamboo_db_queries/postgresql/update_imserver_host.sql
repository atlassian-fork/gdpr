-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "imserver".*,host as host_before,REGEXP_REPLACE(host,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as host_after from imserver where LOWER(host) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update imserver set host = REGEXP_REPLACE(host,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(host) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

