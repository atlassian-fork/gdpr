-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "credentials".*,name as name_before,REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as name_after from credentials where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update credentials set name = REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

