-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(msg) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

