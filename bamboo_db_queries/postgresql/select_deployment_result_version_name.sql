-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : postgresql

select "deployment_result".* from deployment_result where LOWER(version_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

