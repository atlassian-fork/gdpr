-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where DESCRIPTION like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where URL like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

select AO_C7F71E_OAUTH_SVC_PROV_TKNS.* from AO_C7F71E_OAUTH_SVC_PROV_TKNS where VALUE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

select artifact_definition.* from artifact_definition where copy_pattern like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

select artifact_definition.* from artifact_definition where label like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

select artifact_definition.* from artifact_definition where src_directory like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : mssql

select artifact.* from artifact where label like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : mssql

select artifact_subscription.* from artifact_subscription where dst_directory like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

select audit_log.* from audit_log where msg like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

select audit_log.* from audit_log where new_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

select audit_log.* from audit_log where old_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

select audit_log.* from audit_log where task_header like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

select author.* from author where author_email = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

select author.* from author where author_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : mssql

select bandana.* from bandana where serialized_data like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mssql

select brs_consumed_subscription.* from brs_consumed_subscription where dst_directory like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : mssql

select build_definition.* from build_definition where xml_definition_data like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mssql

select build.* from build where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mssql

select build.* from build where title like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

select buildresultsummary_customdata.* from buildresultsummary_customdata where custom_info_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : mssql

select capability.* from capability where value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mssql

select chain_stage.* from chain_stage where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mssql

select chain_stage.* from chain_stage where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

select chain_stage_result.* from chain_stage_result where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

select chain_stage_result.* from chain_stage_result where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mssql

select credentials.* from credentials where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mssql

select credentials.* from credentials where xml like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

select cwd_application_alias.* from cwd_application_alias where alias_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

select cwd_application_alias.* from cwd_application_alias where lower_alias_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mssql

select cwd_application.* from cwd_application where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : mssql

select cwd_directory.* from cwd_directory where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mssql

select cwd_expirable_user_token.* from cwd_expirable_user_token where email_address = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

select cwd_group.* from cwd_group where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where email_address = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where lower_email_address = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mssql

select deployment_env_config.* from deployment_env_config where docker_pipeline_config like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mssql

select deployment_env_config.* from deployment_env_config where plugin_config like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

select deployment_environment.* from deployment_environment where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

select deployment_environment.* from deployment_environment where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

select deployment_environment.* from deployment_environment where triggers_xml_data like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mssql

select deployment_environment.* from deployment_environment where xml_definition_data like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mssql

select deployment_project.* from deployment_project where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : mssql

select deployment_project_item.* from deployment_project_item where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mssql

select deployment_project.* from deployment_project where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : mssql

select deployment_result_customdata.* from deployment_result_customdata where custom_info_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : mssql

select deployment_result.* from deployment_result where version_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mssql

select deployment_variable_substitution.* from deployment_variable_substitution where variable_key like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mssql

select deployment_variable_substitution.* from deployment_variable_substitution where variable_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mssql

select deployment_version_commit.* from deployment_version_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

select deployment_version_item_ba.* from deployment_version_item_ba where copy_pattern like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

select deployment_version_item_ba.* from deployment_version_item_ba where label like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mssql

select deployment_version_item_ba.* from deployment_version_item_ba where location like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : mssql

select deployment_version_item.* from deployment_version_item where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

select deployment_version.* from deployment_version where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : mssql

select deployment_version_naming.* from deployment_version_naming where next_version_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

select deployment_version.* from deployment_version where plan_branch_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mssql

select elastic_image.* from elastic_image where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mssql

select elastic_image.* from elastic_image where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

select groups.* from groups where groupname like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

select imserver.* from imserver where host like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

select imserver.* from imserver where resource_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

select imserver.* from imserver where title like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

select label.* from label where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mssql

select merge_result.* from merge_result where failure_reason like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mssql

select notifications.* from notifications where (recipient like '%<OLD_VALUE>%' ) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

select os_propertyentry.* from os_propertyentry where entity_key like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

select os_propertyentry.* from os_propertyentry where entity_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mssql

select os_propertyentry.* from os_propertyentry where string_val like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mssql

select plan_vcs_history.* from plan_vcs_history where xml_custom_data like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mssql

select project.* from project where description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mssql

select project.* from project where title like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

select queue.* from queue where agent_description like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

select queue.* from queue where title like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

select quick_filter_rules.* from quick_filter_rules where configuration like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

select quick_filter_rules.* from quick_filter_rules where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

select quick_filters.* from quick_filters where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mssql

select requirement.* from requirement where key_identifier like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mssql

select requirement.* from requirement where match_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : mssql

select script.* from script where body like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mssql

select stage_variable_context.* from stage_variable_context where variable_key like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mssql

select stage_variable_context.* from stage_variable_context where variable_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Test error content
-- Database    : mssql

select test_error.* from test_error where error_content like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : mssql

select trusted_apps.* from trusted_apps where app_name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : mssql

select trusted_apps_ips.* from trusted_apps_ips where ip_pattern = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : mssql

select trusted_apps_urls.* from trusted_apps_urls where url_pattern like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mssql

select user_comment.* from user_comment where content like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : mssql

select user_commit.* from user_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mssql

select users.* from users where email = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mssql

select users.* from users where fullname like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

select variable_context.* from variable_context where variable_key like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

select variable_context.* from variable_context where variable_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

select variable_substitution.* from variable_substitution where variable_key like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

select variable_substitution.* from variable_substitution where variable_value like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mssql

select vcs_location.* from vcs_location where name like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mssql

select vcs_location.* from vcs_location where xml_definition_data like '%<OLD_VALUE>%' ;

