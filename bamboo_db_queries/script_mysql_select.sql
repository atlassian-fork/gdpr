-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(URL) like LOWER('%<OLD_VALUE>%') ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mysql

select AO_C7F71E_OAUTH_SVC_PROV_TKNS.* from AO_C7F71E_OAUTH_SVC_PROV_TKNS where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

select artifact_definition.* from artifact_definition where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

select artifact_definition.* from artifact_definition where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

select artifact_definition.* from artifact_definition where LOWER(src_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : mysql

select artifact.* from artifact where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : mysql

select artifact_subscription.* from artifact_subscription where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

select audit_log.* from audit_log where LOWER(msg) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

select audit_log.* from audit_log where LOWER(new_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

select audit_log.* from audit_log where LOWER(old_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

select audit_log.* from audit_log where LOWER(task_header) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mysql

select author.* from author where LOWER(author_email) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mysql

select author.* from author where LOWER(author_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : mysql

select bandana.* from bandana where LOWER(serialized_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mysql

select brs_consumed_subscription.* from brs_consumed_subscription where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : mysql

select build_definition.* from build_definition where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mysql

select build.* from build where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mysql

select build.* from build where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

select buildresultsummary_customdata.* from buildresultsummary_customdata where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : mysql

select capability.* from capability where LOWER(value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mysql

select chain_stage.* from chain_stage where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mysql

select chain_stage.* from chain_stage where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

select chain_stage_result.* from chain_stage_result where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

select chain_stage_result.* from chain_stage_result where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mysql

select credentials.* from credentials where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mysql

select credentials.* from credentials where LOWER(xml) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mysql

select cwd_application_alias.* from cwd_application_alias where LOWER(alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mysql

select cwd_application_alias.* from cwd_application_alias where LOWER(lower_alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mysql

select cwd_application.* from cwd_application where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : mysql

select cwd_directory.* from cwd_directory where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mysql

select cwd_expirable_user_token.* from cwd_expirable_user_token where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mysql

select cwd_group.* from cwd_group where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

select cwd_user.* from cwd_user where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

select cwd_user.* from cwd_user where LOWER(lower_email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mysql

select deployment_env_config.* from deployment_env_config where LOWER(docker_pipeline_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mysql

select deployment_env_config.* from deployment_env_config where LOWER(plugin_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

select deployment_environment.* from deployment_environment where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

select deployment_environment.* from deployment_environment where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

select deployment_environment.* from deployment_environment where LOWER(triggers_xml_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

select deployment_environment.* from deployment_environment where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mysql

select deployment_project.* from deployment_project where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : mysql

select deployment_project_item.* from deployment_project_item where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mysql

select deployment_project.* from deployment_project where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : mysql

select deployment_result_customdata.* from deployment_result_customdata where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : mysql

select deployment_result.* from deployment_result where LOWER(version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mysql

select deployment_variable_substitution.* from deployment_variable_substitution where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mysql

select deployment_variable_substitution.* from deployment_variable_substitution where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mysql

select deployment_version_commit.* from deployment_version_commit where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

select deployment_version_item_ba.* from deployment_version_item_ba where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

select deployment_version_item_ba.* from deployment_version_item_ba where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

select deployment_version_item_ba.* from deployment_version_item_ba where LOWER(location) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : mysql

select deployment_version_item.* from deployment_version_item where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mysql

select deployment_version.* from deployment_version where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : mysql

select deployment_version_naming.* from deployment_version_naming where LOWER(next_version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mysql

select deployment_version.* from deployment_version where LOWER(plan_branch_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mysql

select elastic_image.* from elastic_image where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mysql

select elastic_image.* from elastic_image where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mysql

select groups.* from groups where LOWER(groupname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

select imserver.* from imserver where LOWER(host) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

select imserver.* from imserver where LOWER(resource_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

select imserver.* from imserver where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mysql

select label.* from label where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

select merge_result.* from merge_result where LOWER(failure_reason) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mysql

select notifications.* from notifications where (LOWER(recipient) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

select os_propertyentry.* from os_propertyentry where LOWER(entity_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

select os_propertyentry.* from os_propertyentry where LOWER(entity_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

select os_propertyentry.* from os_propertyentry where LOWER(string_val) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

select plan_vcs_history.* from plan_vcs_history where LOWER(xml_custom_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mysql

select project.* from project where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mysql

select project.* from project where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

select queue.* from queue where LOWER(agent_description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

select queue.* from queue where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

select quick_filter_rules.* from quick_filter_rules where LOWER(configuration) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

select quick_filter_rules.* from quick_filter_rules where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

select quick_filters.* from quick_filters where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

select requirement.* from requirement where LOWER(key_identifier) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

select requirement.* from requirement where LOWER(match_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : mysql

select script.* from script where LOWER(body) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mysql

select stage_variable_context.* from stage_variable_context where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mysql

select stage_variable_context.* from stage_variable_context where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Test error content
-- Database    : mysql

select test_error.* from test_error where LOWER(error_content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : mysql

select trusted_apps.* from trusted_apps where LOWER(app_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : mysql

select trusted_apps_ips.* from trusted_apps_ips where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : mysql

select trusted_apps_urls.* from trusted_apps_urls where LOWER(url_pattern) like LOWER('%<OLD_VALUE>%') ;

-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mysql

select user_comment.* from user_comment where LOWER(content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : mysql

select user_commit.* from user_commit where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mysql

select users.* from users where LOWER(email) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mysql

select users.* from users where LOWER(fullname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

select variable_context.* from variable_context where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

select variable_context.* from variable_context where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

select variable_substitution.* from variable_substitution where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

select variable_substitution.* from variable_substitution where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mysql

select vcs_location.* from vcs_location where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mysql

select vcs_location.* from vcs_location where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

