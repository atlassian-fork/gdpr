-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,triggers_xml_data as triggers_xml_data_before,replace(triggers_xml_data,'<OLD_VALUE>','<NEW_VALUE>') as triggers_xml_data_after from deployment_environment where LOWER(triggers_xml_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_environment set triggers_xml_data = replace(triggers_xml_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(triggers_xml_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

