-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select groups.*,groupname as groupname_before,replace(groupname,'<OLD_VALUE>','<NEW_VALUE>') as groupname_after from groups where LOWER(groupname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update groups set groupname = replace(groupname,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(groupname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

