-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') as xml_definition_data_after from deployment_environment where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_environment set xml_definition_data = replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

