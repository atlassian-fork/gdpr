-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mysql

select vcs_location.* from vcs_location where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

