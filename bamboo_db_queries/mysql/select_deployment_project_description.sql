-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mysql

select deployment_project.* from deployment_project where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

