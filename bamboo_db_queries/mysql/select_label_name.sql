-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mysql

select label.* from label where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

