-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,configuration as configuration_before,replace(configuration,'<OLD_VALUE>','<NEW_VALUE>') as configuration_after from quick_filter_rules where LOWER(configuration) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update quick_filter_rules set configuration = replace(configuration,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(configuration) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

