-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,docker_pipeline_config as docker_pipeline_config_before,replace(docker_pipeline_config,'<OLD_VALUE>','<NEW_VALUE>') as docker_pipeline_config_after from deployment_env_config where LOWER(docker_pipeline_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_env_config set docker_pipeline_config = replace(docker_pipeline_config,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(docker_pipeline_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

