-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mysql

select cwd_application_alias.* from cwd_application_alias where LOWER(alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

