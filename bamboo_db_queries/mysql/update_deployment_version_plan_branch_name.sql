-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,plan_branch_name as plan_branch_name_before,replace(plan_branch_name,'<OLD_VALUE>','<NEW_VALUE>') as plan_branch_name_after from deployment_version where LOWER(plan_branch_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version set plan_branch_name = replace(plan_branch_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(plan_branch_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

