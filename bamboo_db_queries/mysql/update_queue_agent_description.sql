-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select queue.*,agent_description as agent_description_before,replace(agent_description,'<OLD_VALUE>','<NEW_VALUE>') as agent_description_after from queue where LOWER(agent_description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update queue set agent_description = replace(agent_description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(agent_description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

