-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

select imserver.* from imserver where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

