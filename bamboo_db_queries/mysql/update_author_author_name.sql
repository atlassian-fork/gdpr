-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select author.*,author_name as author_name_before,replace(author_name,'<OLD_VALUE>','<NEW_VALUE>') as author_name_after from author where LOWER(author_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update author set author_name = replace(author_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(author_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

