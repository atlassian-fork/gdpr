-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : mysql

select deployment_project_item.* from deployment_project_item where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

