-- Type        : select
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : mysql

select script.* from script where LOWER(body) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

