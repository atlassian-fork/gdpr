-- Type        : update
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select chain_stage.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from chain_stage where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update chain_stage set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

