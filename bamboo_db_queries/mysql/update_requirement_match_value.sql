-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,match_value as match_value_before,replace(match_value,'<OLD_VALUE>','<NEW_VALUE>') as match_value_after from requirement where LOWER(match_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update requirement set match_value = replace(match_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(match_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

