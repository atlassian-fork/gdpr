-- Type        : select
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : mysql

select capability.* from capability where LOWER(value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

