-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : mysql

select cwd_directory.* from cwd_directory where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

