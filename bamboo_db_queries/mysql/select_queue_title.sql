-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

select queue.* from queue where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

