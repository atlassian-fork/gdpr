-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

select variable_context.* from variable_context where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

