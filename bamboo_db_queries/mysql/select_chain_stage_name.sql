-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mysql

select chain_stage.* from chain_stage where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

