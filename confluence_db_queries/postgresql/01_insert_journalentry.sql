-- Type       : insert
-- Origin     : Journal Entry
-- Description: Inserting Journal Entry rows to trigger incremental index
INSERT INTO journalentry
                      SELECT nextval('seq_journal_entry_id')                                            AS entry_id
                        ,    'main_index'                                                               AS journal_name
                        ,    (current_timestamp(0) - INTERVAL '2 day') :: TIMESTAMP                     AS creationdate
                        ,    'DELETE_DOCUMENT'                                                          AS type
                        ,    concat('com.atlassian.confluence.user.PersonalInformation-', C2.CONTENTID) AS message
                      FROM (
                             SELECT CONTENTID
                             FROM CONTENT
                             WHERE CONTENTTYPE = 'USERINFO' AND CONTENT.USERNAME IN (
                               SELECT user_key
                               FROM user_mapping
                               WHERE username = '__username__')) AS C2;

INSERT INTO journalentry
                      SELECT nextval('seq_journal_entry_id')                                            AS entry_id
                        ,    'main_index'                                                               AS journal_name
                        ,    (current_timestamp(0) - INTERVAL '2 day') :: TIMESTAMP                     AS creationdate
                        ,    'DELETE_CHANGE_DOCUMENTS'                                                  AS type
                        ,    concat('com.atlassian.confluence.user.PersonalInformation-', C2.CONTENTID) AS message
                      FROM (
                             SELECT CONTENTID
                             FROM CONTENT
                             WHERE CONTENTTYPE = 'USERINFO' AND CONTENT.USERNAME IN (
                               SELECT user_key
                               FROM user_mapping
                               WHERE username = '__username__')) AS C2;

INSERT INTO journalentry
                      SELECT nextval('seq_journal_entry_id') AS entry_id
                        ,    'main_index'                    AS journal_name
                        ,    (current_timestamp(0) - INTERVAL '2 day') :: TIMESTAMP AS creationdate
                        ,    type_name                       AS type
                        ,    CASE
                            WHEN (C2.CONTENTTYPE = 'PAGE')
                                THEN concat('com.atlassian.confluence.pages.Page-', C2.CONTENTID)
                            WHEN (C2.CONTENTTYPE = 'BLOGPOST')
                                THEN concat('com.atlassian.confluence.pages.BlogPost-', C2.CONTENTID)
                            WHEN (C2.CONTENTTYPE = 'COMMENT')
                                THEN concat('com.atlassian.confluence.pages.Comment-', C2.CONTENTID)
                            END               AS message
                        FROM (
                            SELECT CONTENT.CONTENTID
                                , CONTENT.CONTENTTYPE
                            FROM CONTENT
                                INNER JOIN BODYCONTENT
                                ON CONTENT.CONTENTID = BODYCONTENT.CONTENTID
                            WHERE CONTENTTYPE IN ('PAGE', 'BLOGPOST', 'COMMENT')
                                    AND CONTENT.PREVVER IS NULL
                                    AND CONTENT.CONTENT_STATUS = 'current'
                                    AND BODYCONTENT.BODY LIKE CONCAT('%<ri:user ri:userkey="', (
                                SELECT user_key
                                FROM user_mapping
                                WHERE user_mapping.username = '__username__'), '"%')
                            ) AS C2
                        JOIN (SELECT 'ADD_CHANGE_DOCUMENT' AS type_name
                                UNION SELECT 'UPDATE_DOCUMENT' AS type_name) AS tmp_type
                            ON (1 = 1);