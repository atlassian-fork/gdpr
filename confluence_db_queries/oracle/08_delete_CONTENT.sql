-- Type       : delete
-- Origin     : User Profile Info History
-- Description: Deleting User Profile Info History
delete from CONTENT where PREVVER IN (select CONTENTID from CONTENT where CONTENTTYPE = 'USERINFO' and CONTENT.USERNAME IN (select user_key from user_mapping where username = '__username__' ));