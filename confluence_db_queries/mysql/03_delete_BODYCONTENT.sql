-- Type       : delete
-- Origin     : User Profile Info
-- Description: Deleting User Profile Info
delete from BODYCONTENT where BODYCONTENT.CONTENTID IN (select CONTENTID from CONTENT where CONTENT.CONTENTTYPE = 'USERINFO' and CONTENT.USERNAME IN (select user_key from user_mapping where username = '__username__' ));