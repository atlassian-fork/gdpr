-- Type        : update
-- Origin      : jira mail plugin
-- Description : mail loop detection when handling issues
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_3B1893_LOOP_DETECTION.*,SENDER_EMAIL as SENDER_EMAIL_before,'<NEW_PD_VALUE>' as SENDER_EMAIL_after from AO_3B1893_LOOP_DETECTION where SENDER_EMAIL = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update AO_3B1893_LOOP_DETECTION set SENDER_EMAIL = '<NEW_PD_VALUE>' where SENDER_EMAIL = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_4AEACD_WEBHOOK_DAO.*,FILTER as FILTER_before,replace(FILTER, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as FILTER_after from AO_4AEACD_WEBHOOK_DAO where FILTER like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_4AEACD_WEBHOOK_DAO set FILTER = replace(FILTER, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where FILTER like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_4AEACD_WEBHOOK_DAO.*,PARAMETERS as PARAMETERS_before,replace(PARAMETERS, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as PARAMETERS_after from AO_4AEACD_WEBHOOK_DAO where PARAMETERS like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_4AEACD_WEBHOOK_DAO set PARAMETERS = replace(PARAMETERS, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where PARAMETERS like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : project shortcuts
-- Description : project shortcut
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}/summary, where PKEY: select pkey from project where id = PROJECT_ID;
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'Projects' top menu
--     3. Select specific project name
--     4. 'Project Shortcuts' section (left sidebar)
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_550953_SHORTCUT.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_550953_SHORTCUT where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_550953_SHORTCUT set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : project shortcuts
-- Description : project shortcut
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}/summary, where PKEY: select pkey from project where id = PROJECT_ID;
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'Projects' top menu
--     3. Select specific project name
--     4. 'Project Shortcuts' section (left sidebar)
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_550953_SHORTCUT.*,SHORTCUT_URL as SHORTCUT_URL_before,'<NEW_PD_VALUE>' as SHORTCUT_URL_after from AO_550953_SHORTCUT where SHORTCUT_URL like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_550953_SHORTCUT set SHORTCUT_URL = '<NEW_PD_VALUE>' where SHORTCUT_URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTIVITY_ENTITY.*,CONTENT as CONTENT_before,replace(CONTENT, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as CONTENT_after from AO_563AEE_ACTIVITY_ENTITY where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_ACTIVITY_ENTITY set CONTENT = replace(CONTENT, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTIVITY_ENTITY.*,TITLE as TITLE_before,replace(TITLE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as TITLE_after from AO_563AEE_ACTIVITY_ENTITY where TITLE like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_ACTIVITY_ENTITY set TITLE = replace(TITLE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where TITLE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTIVITY_ENTITY.*,URL as URL_before,'<NEW_PD_VALUE>' as URL_after from AO_563AEE_ACTIVITY_ENTITY where URL like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_ACTIVITY_ENTITY set URL = '<NEW_PD_VALUE>' where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTOR_ENTITY.*,FULL_NAME as FULL_NAME_before,replace(FULL_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as FULL_NAME_after from AO_563AEE_ACTOR_ENTITY where FULL_NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_ACTOR_ENTITY set FULL_NAME = replace(FULL_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where FULL_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTOR_ENTITY.*,PROFILE_PAGE_URI as PROFILE_PAGE_URI_before,'<NEW_PD_VALUE>' as PROFILE_PAGE_URI_after from AO_563AEE_ACTOR_ENTITY where PROFILE_PAGE_URI like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_ACTOR_ENTITY set PROFILE_PAGE_URI = '<NEW_PD_VALUE>' where PROFILE_PAGE_URI like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_ACTOR_ENTITY.*,PROFILE_PICTURE_URI as PROFILE_PICTURE_URI_before,'<NEW_PD_VALUE>' as PROFILE_PICTURE_URI_after from AO_563AEE_ACTOR_ENTITY where PROFILE_PICTURE_URI like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_ACTOR_ENTITY set PROFILE_PICTURE_URI = '<NEW_PD_VALUE>' where PROFILE_PICTURE_URI like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : link to some media eg. image
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_MEDIA_LINK_ENTITY.*,URL as URL_before,'<NEW_PD_VALUE>' as URL_after from AO_563AEE_MEDIA_LINK_ENTITY where URL like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_MEDIA_LINK_ENTITY set URL = '<NEW_PD_VALUE>' where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_OBJECT_ENTITY.*,CONTENT as CONTENT_before,replace(CONTENT, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as CONTENT_after from AO_563AEE_OBJECT_ENTITY where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_OBJECT_ENTITY set CONTENT = replace(CONTENT, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_OBJECT_ENTITY.*,DISPLAY_NAME as DISPLAY_NAME_before,replace(DISPLAY_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DISPLAY_NAME_after from AO_563AEE_OBJECT_ENTITY where DISPLAY_NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_OBJECT_ENTITY set DISPLAY_NAME = replace(DISPLAY_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DISPLAY_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_OBJECT_ENTITY.*,SUMMARY as SUMMARY_before,replace(SUMMARY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as SUMMARY_after from AO_563AEE_OBJECT_ENTITY where SUMMARY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_OBJECT_ENTITY set SUMMARY = replace(SUMMARY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where SUMMARY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_TARGET_ENTITY.*,CONTENT as CONTENT_before,replace(CONTENT, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as CONTENT_after from AO_563AEE_TARGET_ENTITY where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_TARGET_ENTITY set CONTENT = replace(CONTENT, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_TARGET_ENTITY.*,DISPLAY_NAME as DISPLAY_NAME_before,replace(DISPLAY_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DISPLAY_NAME_after from AO_563AEE_TARGET_ENTITY where DISPLAY_NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_TARGET_ENTITY set DISPLAY_NAME = replace(DISPLAY_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DISPLAY_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_563AEE_TARGET_ENTITY.*,SUMMARY as SUMMARY_before,replace(SUMMARY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as SUMMARY_after from AO_563AEE_TARGET_ENTITY where SUMMARY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_563AEE_TARGET_ENTITY set SUMMARY = replace(SUMMARY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where SUMMARY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : hipchat integration plugin
-- Description : seems to be auth data for specific users - ability to use private rooms in hipchat
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_5FB9D7_AOHIP_CHAT_USER.*,HIP_CHAT_USER_NAME as HIP_CHAT_USER_NAME_before,replace(HIP_CHAT_USER_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as HIP_CHAT_USER_NAME_after from AO_5FB9D7_AOHIP_CHAT_USER where HIP_CHAT_USER_NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_5FB9D7_AOHIP_CHAT_USER set HIP_CHAT_USER_NAME = replace(HIP_CHAT_USER_NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where HIP_CHAT_USER_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : spring audit log (reopen/close sprint)
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_60DB71_AUDITENTRY.*,DATA as DATA_before,replace(DATA, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DATA_after from AO_60DB71_AUDITENTRY where DATA like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_AUDITENTRY set DATA = replace(DATA, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DATA like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : board column name
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=columns
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Columns'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_COLUMN.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_60DB71_COLUMN where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_COLUMN set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_QUICKFILTER.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DESCRIPTION_after from AO_60DB71_QUICKFILTER where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_QUICKFILTER set DESCRIPTION = replace(DESCRIPTION, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_QUICKFILTER.*,LONG_QUERY as LONG_QUERY_before,replace(LONG_QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as LONG_QUERY_after from AO_60DB71_QUICKFILTER where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_QUICKFILTER set LONG_QUERY = replace(LONG_QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_QUICKFILTER.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_60DB71_QUICKFILTER where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_QUICKFILTER set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_QUICKFILTER.*,QUERY as QUERY_before,replace(QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as QUERY_after from AO_60DB71_QUICKFILTER where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_QUICKFILTER set QUERY = replace(QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : board
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_RAPIDVIEW.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_60DB71_RAPIDVIEW where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_RAPIDVIEW set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : sprint
-- Table valid only for specific product : Jira Software
-- Table column valid only for specific versions : Jira>=7.5, Jira ServiceDesk>=3.8.1
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidBoard.jspa?rapidView=${RAPID_VIEW_ID}&view=planning.nodetail
-- 
--   How to access: 
--     1. ? Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SPRINT.*,GOAL as GOAL_before,replace(GOAL, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as GOAL_after from AO_60DB71_SPRINT where GOAL like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_SPRINT set GOAL = replace(GOAL, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where GOAL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : sprint
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidBoard.jspa?rapidView=${RAPID_VIEW_ID}&view=planning.nodetail
-- 
--   How to access: 
--     1. ? Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SPRINT.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_60DB71_SPRINT where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update AO_60DB71_SPRINT set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : kanban filter subquery
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SUBQUERY.*,LONG_QUERY as LONG_QUERY_before,replace(LONG_QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as LONG_QUERY_after from AO_60DB71_SUBQUERY where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_SUBQUERY set LONG_QUERY = replace(LONG_QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : kanban filter subquery
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SUBQUERY.*,QUERY as QUERY_before,replace(QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as QUERY_after from AO_60DB71_SUBQUERY where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_SUBQUERY set QUERY = replace(QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SWIMLANE.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DESCRIPTION_after from AO_60DB71_SWIMLANE where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_SWIMLANE set DESCRIPTION = replace(DESCRIPTION, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SWIMLANE.*,LONG_QUERY as LONG_QUERY_before,replace(LONG_QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as LONG_QUERY_after from AO_60DB71_SWIMLANE where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_SWIMLANE set LONG_QUERY = replace(LONG_QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SWIMLANE.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_60DB71_SWIMLANE where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_SWIMLANE set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_60DB71_SWIMLANE.*,QUERY as QUERY_before,replace(QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as QUERY_after from AO_60DB71_SWIMLANE where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_60DB71_SWIMLANE set QUERY = replace(QUERY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira inform plugin
-- Description : saved issue event data
-- Table valid only for specific versions : Jira>=8.0, Jira ServiceDesk>=4.0.0
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_733371_EVENT_PARAMETER.*,VALUE as VALUE_before,replace(VALUE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as VALUE_after from AO_733371_EVENT_PARAMETER where VALUE like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_733371_EVENT_PARAMETER set VALUE = replace(VALUE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where VALUE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : invite users plugin
-- Description : invitation to jira
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_97EDAB_USERINVITATION.*,EMAIL_ADDRESS as EMAIL_ADDRESS_before,'<NEW_PD_VALUE>' as EMAIL_ADDRESS_after from AO_97EDAB_USERINVITATION where EMAIL_ADDRESS = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update AO_97EDAB_USERINVITATION set EMAIL_ADDRESS = '<NEW_PD_VALUE>' where EMAIL_ADDRESS = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DESCRIPTION_after from AO_A0B856_WEB_HOOK_LISTENER_AO where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set DESCRIPTION = replace(DESCRIPTION, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_A0B856_WEB_HOOK_LISTENER_AO where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,URL as URL_before,'<NEW_PD_VALUE>' as URL_after from AO_A0B856_WEB_HOOK_LISTENER_AO where URL like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set URL = '<NEW_PD_VALUE>' where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira diagnostics plugin
-- Description : saved alerts
-- Table valid only for specific versions : Jira>=7.13, Jira ServiceDesk>=3.16.0
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_C16815_ALERT_AO.*,DETAILS_JSON as DETAILS_JSON_before,replace(DETAILS_JSON, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DETAILS_JSON_after from AO_C16815_ALERT_AO where DETAILS_JSON like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_C16815_ALERT_AO set DETAILS_JSON = replace(DETAILS_JSON, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DETAILS_JSON like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_CHANGESET_MAPPING.*,AUTHOR as AUTHOR_before,replace(AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as AUTHOR_after from AO_E8B6CC_CHANGESET_MAPPING where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_CHANGESET_MAPPING set AUTHOR = replace(AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_CHANGESET_MAPPING.*,AUTHOR_EMAIL as AUTHOR_EMAIL_before,'<NEW_PD_VALUE>' as AUTHOR_EMAIL_after from AO_E8B6CC_CHANGESET_MAPPING where AUTHOR_EMAIL = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update AO_E8B6CC_CHANGESET_MAPPING set AUTHOR_EMAIL = '<NEW_PD_VALUE>' where AUTHOR_EMAIL = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_CHANGESET_MAPPING.*,BRANCH as BRANCH_before,replace(BRANCH, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as BRANCH_after from AO_E8B6CC_CHANGESET_MAPPING where BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_CHANGESET_MAPPING set BRANCH = replace(BRANCH, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_CHANGESET_MAPPING.*,MESSAGE as MESSAGE_before,replace(MESSAGE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as MESSAGE_after from AO_E8B6CC_CHANGESET_MAPPING where MESSAGE like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_CHANGESET_MAPPING set MESSAGE = replace(MESSAGE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where MESSAGE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_CHANGESET_MAPPING.*,RAW_AUTHOR as RAW_AUTHOR_before,replace(RAW_AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as RAW_AUTHOR_after from AO_E8B6CC_CHANGESET_MAPPING where RAW_AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_CHANGESET_MAPPING set RAW_AUTHOR = replace(RAW_AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where RAW_AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_COMMIT.*,AUTHOR as AUTHOR_before,replace(AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as AUTHOR_after from AO_E8B6CC_COMMIT where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_COMMIT set AUTHOR = replace(AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_COMMIT.*,MESSAGE as MESSAGE_before,replace(MESSAGE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as MESSAGE_after from AO_E8B6CC_COMMIT where MESSAGE like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_COMMIT set MESSAGE = replace(MESSAGE, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where MESSAGE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_COMMIT.*,RAW_AUTHOR as RAW_AUTHOR_before,replace(RAW_AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as RAW_AUTHOR_after from AO_E8B6CC_COMMIT where RAW_AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_COMMIT set RAW_AUTHOR = replace(RAW_AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where RAW_AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : PR participants
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PR_PARTICIPANT.*,USERNAME as USERNAME_before,replace(USERNAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as USERNAME_after from AO_E8B6CC_PR_PARTICIPANT where USERNAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PR_PARTICIPANT set USERNAME = replace(USERNAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where USERNAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PULL_REQUEST.*,AUTHOR as AUTHOR_before,replace(AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as AUTHOR_after from AO_E8B6CC_PULL_REQUEST where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PULL_REQUEST set AUTHOR = replace(AUTHOR, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PULL_REQUEST.*,DESTINATION_BRANCH as DESTINATION_BRANCH_before,replace(DESTINATION_BRANCH, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as DESTINATION_BRANCH_after from AO_E8B6CC_PULL_REQUEST where DESTINATION_BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PULL_REQUEST set DESTINATION_BRANCH = replace(DESTINATION_BRANCH, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where DESTINATION_BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PULL_REQUEST.*,EXECUTED_BY as EXECUTED_BY_before,replace(EXECUTED_BY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as EXECUTED_BY_after from AO_E8B6CC_PULL_REQUEST where EXECUTED_BY like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PULL_REQUEST set EXECUTED_BY = replace(EXECUTED_BY, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where EXECUTED_BY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PULL_REQUEST.*,NAME as NAME_before,replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as NAME_after from AO_E8B6CC_PULL_REQUEST where NAME like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PULL_REQUEST set NAME = replace(NAME, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PULL_REQUEST.*,SOURCE_BRANCH as SOURCE_BRANCH_before,replace(SOURCE_BRANCH, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as SOURCE_BRANCH_after from AO_E8B6CC_PULL_REQUEST where SOURCE_BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PULL_REQUEST set SOURCE_BRANCH = replace(SOURCE_BRANCH, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where SOURCE_BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PULL_REQUEST.*,SOURCE_REPO as SOURCE_REPO_before,replace(SOURCE_REPO, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as SOURCE_REPO_after from AO_E8B6CC_PULL_REQUEST where SOURCE_REPO like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PULL_REQUEST set SOURCE_REPO = replace(SOURCE_REPO, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where SOURCE_REPO like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_E8B6CC_PULL_REQUEST.*,URL as URL_before,'<NEW_PD_VALUE>' as URL_after from AO_E8B6CC_PULL_REQUEST where URL like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_E8B6CC_PULL_REQUEST set URL = '<NEW_PD_VALUE>' where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : application user
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${lower_user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select app_user.*,lower_user_name as lower_user_name_before,'<NEW_PD_VALUE>' as lower_user_name_after from app_user where lower_user_name = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update app_user set lower_user_name = '<NEW_PD_VALUE>' where lower_user_name = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : audit log changed value
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_changed_value where delta_from like '%<CURRENT_PD_VALUE>%' );
select audit_changed_value.*,delta_from as delta_from_before,cast(replace(cast(delta_from as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as delta_from_after from audit_changed_value where delta_from like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--  updating dependant data
update audit_log set search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where id in (select log_id from audit_changed_value where delta_from like '%<CURRENT_PD_VALUE>%' );
update audit_changed_value set delta_from = cast(replace(cast(delta_from as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where delta_from like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : audit log changed value
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_changed_value where delta_to like '%<CURRENT_PD_VALUE>%' );
select audit_changed_value.*,delta_to as delta_to_before,cast(replace(cast(delta_to as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as delta_to_after from audit_changed_value where delta_to like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--  updating dependant data
update audit_log set search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where id in (select log_id from audit_changed_value where delta_to like '%<CURRENT_PD_VALUE>%' );
update audit_changed_value set delta_to = cast(replace(cast(delta_to as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where delta_to like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select audit_item.*,object_id as object_id_before,'<NEW_PD_VALUE>' as object_id_after from audit_item where object_id like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_item set object_id = '<NEW_PD_VALUE>' where object_id like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_item where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN'));
select audit_item.*,object_name as object_name_before,'<NEW_PD_VALUE>' as object_name_after from audit_item where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- + UPDATE (be careful)
--  updating dependant data
update audit_log set search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where id in (select log_id from audit_item where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN'));
update audit_item set object_name = '<NEW_PD_VALUE>' where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : update
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_item where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER');
select audit_item.*,object_name as object_name_before,'<NEW_PD_VALUE>' as object_name_after from audit_item where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER';

-- + UPDATE (be careful)
--  updating dependant data
update audit_log set search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where id in (select log_id from audit_item where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER');
update audit_item set object_name = '<NEW_PD_VALUE>' where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER';

-- Type        : update
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select audit_log.*,object_id as object_id_before,replace(object_id, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as object_id_after from audit_log where (object_id like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- + UPDATE (be careful)
update audit_log set object_id = replace(object_id, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where (object_id like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : update
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select audit_log.*,object_name as object_name_before,replace(object_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as object_name_after,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- + UPDATE (be careful)
update audit_log set object_name = replace(object_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>'),search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : update
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select audit_log.*,object_name as object_name_before,'<NEW_PD_VALUE>' as object_name_after,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER';

-- + UPDATE (be careful)
update audit_log set object_name = '<NEW_PD_VALUE>',search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER';

-- Type        : update
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select audit_log.*,remote_address as remote_address_before,'<NEW_PD_VALUE>' as remote_address_after,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where (remote_address = '<CURRENT_PD_VALUE>' ) AND remote_address  IS NOT NULL;

-- + UPDATE (be careful)
update audit_log set remote_address = '<NEW_PD_VALUE>',search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where (remote_address = '<CURRENT_PD_VALUE>' ) AND remote_address  IS NOT NULL;

-- Type        : update
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where search_field like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update audit_log set search_field = cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where search_field like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : avatar
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/ViewProfile.jspa?name=${user_key}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select avatar.*,filename as filename_before,replace(filename, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as filename_after from avatar where filename like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update avatar set filename = replace(filename, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where filename like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select changeitem.*,field as field_before,replace(field, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as field_after from changeitem where field like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update changeitem set field = replace(field, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where field like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select changeitem.*,newstring as newstring_before,cast(replace(cast(newstring as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as newstring_after from changeitem where newstring like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update changeitem set newstring = cast(replace(cast(newstring as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where newstring like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select changeitem.*,newvalue as newvalue_before,cast(replace(cast(newvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as newvalue_after from changeitem where newvalue like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update changeitem set newvalue = cast(replace(cast(newvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where newvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select changeitem.*,oldstring as oldstring_before,cast(replace(cast(oldstring as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as oldstring_after from changeitem where oldstring like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update changeitem set oldstring = cast(replace(cast(oldstring as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where oldstring like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select changeitem.*,oldvalue as oldvalue_before,cast(replace(cast(oldvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as oldvalue_after from changeitem where oldvalue like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update changeitem set oldvalue = cast(replace(cast(oldvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where oldvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : component
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select component.*,cname as cname_before,replace(cname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as cname_after from component where cname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update component set cname = replace(cname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where cname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : component
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select component.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from component where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update component set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : custom field
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditCustomField!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select customfield.*,cfname as cfname_before,replace(cfname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as cfname_after from customfield where cfname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update customfield set cfname = replace(cfname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where cfname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : custom field
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditCustomField!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select customfield.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from customfield where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update customfield set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : customfield value
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select customfieldvalue.*,stringvalue as stringvalue_before,replace(stringvalue, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as stringvalue_after from customfieldvalue where stringvalue like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update customfieldvalue set stringvalue = replace(stringvalue, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where stringvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : customfield value
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select customfieldvalue.*,textvalue as textvalue_before,cast(replace(cast(textvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as textvalue_after from customfieldvalue where textvalue like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update customfieldvalue set textvalue = cast(replace(cast(textvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where textvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : group membership
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/EditUserGroups!default.jspa?name=${child_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Groups'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/group
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,child_name as child_name_before,'<NEW_PD_VALUE>' as child_name_after from cwd_membership where child_name = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_membership set child_name = '<NEW_PD_VALUE>' where child_name = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : group membership
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/EditUserGroups!default.jspa?name=${child_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Groups'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/group
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,lower_child_name as lower_child_name_before,'<NEW_PD_VALUE>' as lower_child_name_after from cwd_membership where lower_child_name = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_membership set lower_child_name = '<NEW_PD_VALUE>' where lower_child_name = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,display_name as display_name_before,replace(display_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as display_name_after from cwd_user where display_name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_user set display_name = replace(display_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where display_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,email_address as email_address_before,'<NEW_PD_VALUE>' as email_address_after from cwd_user where email_address = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set email_address = '<NEW_PD_VALUE>' where email_address = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,first_name as first_name_before,replace(first_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as first_name_after from cwd_user where first_name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_user set first_name = replace(first_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where first_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,last_name as last_name_before,replace(last_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as last_name_after from cwd_user where last_name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_user set last_name = replace(last_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where last_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_display_name as lower_display_name_before,replace(lower_display_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as lower_display_name_after from cwd_user where lower_display_name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_user set lower_display_name = replace(lower_display_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where lower_display_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_email_address as lower_email_address_before,'<NEW_PD_VALUE>' as lower_email_address_after from cwd_user where lower_email_address = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_email_address = '<NEW_PD_VALUE>' where lower_email_address = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_first_name as lower_first_name_before,replace(lower_first_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as lower_first_name_after from cwd_user where lower_first_name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_user set lower_first_name = replace(lower_first_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where lower_first_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_last_name as lower_last_name_before,replace(lower_last_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as lower_last_name_after from cwd_user where lower_last_name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_user set lower_last_name = replace(lower_last_name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where lower_last_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_user_name as lower_user_name_before,'<NEW_PD_VALUE>' as lower_user_name_after from cwd_user where lower_user_name = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_user_name = '<NEW_PD_VALUE>' where lower_user_name = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select cwd_user.*,user_name as user_name_before,'<NEW_PD_VALUE>' as user_name_after from cwd_user where user_name = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update cwd_user set user_name = '<NEW_PD_VALUE>' where user_name = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select draftworkflowscheme.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from draftworkflowscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update draftworkflowscheme set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select draftworkflowscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from draftworkflowscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update draftworkflowscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : additional entity properties
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select entity_property.*,json_value as json_value_before,cast(replace(cast(json_value as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as json_value_after from entity_property where json_value like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update entity_property set json_value = cast(replace(cast(json_value as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where json_value like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : field configuration context
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId={CF_ID} where CF_ID: split fieldid on _
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldconfigscheme.*,configname as configname_before,replace(configname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as configname_after from fieldconfigscheme where configname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldconfigscheme set configname = replace(configname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where configname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : field configuration context
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId={CF_ID} where CF_ID: split fieldid on _
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldconfigscheme.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from fieldconfigscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldconfigscheme set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : field configuration
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayout!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldlayout.*,description as description_before,replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as description_after from fieldlayout where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldlayout set description = replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : field configuration
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayout!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldlayout.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from fieldlayout where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldlayout set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : single field configuration on specific field configuration
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureFieldLayout!default.jspa?id=${fieldlayout} and search for fieldidentifier
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldlayoutitem.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from fieldlayoutitem where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldlayoutitem set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : field config scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayoutScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
--     6. Click on a specific field configuration name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldlayoutscheme.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from fieldlayoutscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldlayoutscheme set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : field config scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayoutScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
--     6. Click on a specific field configuration name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldlayoutscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from fieldlayoutscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldlayoutscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : screen
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
--     7. Click on a 'Screens' under specific field name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldscreen.*,description as description_before,replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as description_after from fieldscreen where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldscreen set description = replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : screen
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
--     7. Click on a 'Screens' under specific field name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldscreen.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from fieldscreen where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldscreen set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Screens schemes'
--     6. Click 'Edit' under specific screen scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldscreenscheme.*,description as description_before,replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as description_after from fieldscreenscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldscreenscheme set description = replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Screens schemes'
--     6. Click 'Edit' under specific screen scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldscreenscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from fieldscreenscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldscreenscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : screen tab
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${fieldscreen}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Click 'Edit' under specific screen name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldscreentab.*,description as description_before,replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as description_after from fieldscreentab where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldscreentab set description = replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : screen tab
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${fieldscreen}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Click 'Edit' under specific screen name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fieldscreentab.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from fieldscreentab where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fieldscreentab set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : attachment
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Attachments' sections
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select fileattachment.*,filename as filename_before,replace(filename, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as filename_after from fileattachment where filename like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update fileattachment set filename = replace(filename, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where filename like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : custom field default value
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId=${CUSTOM_FIELD_ID} where CUSTOM_FIELD_ID: select SUBSTRING(fieldid, 13) from fieldconfiguration where id = ${datakey};
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select genericconfiguration.*,xmlvalue as xmlvalue_before,cast(replace(cast(xmlvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as xmlvalue_after from genericconfiguration where xmlvalue like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update genericconfiguration set xmlvalue = cast(replace(cast(xmlvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where xmlvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue security scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurityScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue Security Schemes'
--     5. Click 'Edit' under specific issue security scheme name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuesecurityscheme.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from issuesecurityscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update issuesecurityscheme set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue security scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurityScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue Security Schemes'
--     5. Click 'Edit' under specific issue security scheme name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuesecurityscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from issuesecurityscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update issuesecurityscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue status
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditStatus!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' (left sidebar)
--     5. Choose 'Status'
--     6. Click 'Edit' under specific issue status name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuestatus.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from issuestatus where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update issuestatus set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue status
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditStatus!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' (left sidebar)
--     5. Choose 'Status'
--     6. Click 'Edit' under specific issue status name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuestatus.*,pname as pname_before,replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as pname_after from issuestatus where pname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update issuestatus set pname = replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue type
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueType!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Types' (left sidebar)
--     5. Choose 'Issue Types'
--     6. Click 'Edit' under specific issue type name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issuetype
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuetype.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from issuetype where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update issuetype set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue type
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueType!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Types' (left sidebar)
--     5. Choose 'Issue Types'
--     6. Click 'Edit' under specific issue type name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issuetype
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuetype.*,pname as pname_before,replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as pname_after from issuetype where pname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update issuetype set pname = replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue type screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueTypeScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Issue type screen schemes'
--     6. Click 'Edit' under specific issue type screen scheme name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuetypescreenscheme.*,description as description_before,replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as description_after from issuetypescreenscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update issuetypescreenscheme set description = replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue type screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueTypeScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Issue type screen schemes'
--     6. Click 'Edit' under specific issue type screen scheme name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select issuetypescreenscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from issuetypescreenscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update issuetypescreenscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : comment
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Comments' tab
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraaction.*,actionbody as actionbody_before,cast(replace(cast(actionbody as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as actionbody_after from jiraaction where actionbody like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update jiraaction set actionbody = cast(replace(cast(actionbody as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where actionbody like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : draft of workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${parentname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiradraftworkflows.*,descriptor as descriptor_before,cast(replace(cast(descriptor as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as descriptor_after from jiradraftworkflows where descriptor like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update jiradraftworkflows set descriptor = cast(replace(cast(descriptor as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where descriptor like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : draft of workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${parentname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiradraftworkflows.*,parentname as parentname_before,replace(parentname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as parentname_after from jiradraftworkflows where parentname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update jiradraftworkflows set parentname = replace(parentname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where parentname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraissue.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from jiraissue where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update jiraissue set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraissue.*,environment as environment_before,cast(replace(cast(environment as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as environment_after from jiraissue where environment like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update jiraissue set environment = cast(replace(cast(environment as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where environment like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraissue.*,summary as summary_before,replace(summary, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as summary_after from jiraissue where summary like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update jiraissue set summary = replace(summary, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where summary like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${workflowname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select jiraworkflows.*,descriptor as descriptor_before,cast(replace(cast(descriptor as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as descriptor_after from jiraworkflows where descriptor like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update jiraworkflows set descriptor = cast(replace(cast(descriptor as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where descriptor like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${workflowname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
-- 
-- SQL update of workflow name is not supported because it could break JIRA
-- Please follow steps described below
-- If it contains personal data please copy it and give it a new name
-- Assign new workflow in all workflow schemes using old workflow
-- Delete old workflow
select jiraworkflows.*,workflowname as workflowname_before,replace(workflowname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as workflowname_after from jiraworkflows where workflowname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
-- SQL update of workflow name is not supported because it could break JIRA
-- Please follow steps described below
-- If it contains personal data please copy it and give it a new name
-- Assign new workflow in all workflow schemes using old workflow
-- Delete old workflow
 
 
-- Type        : update
-- Origin      : jira
-- Description : issue label
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Details' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select label.*,label as label_before,replace(label, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as label_after from label where label like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update label set label = replace(label, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where label like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : seems to be related to mail handler eg. issue created from email, issue commented from email etc.
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select notificationinstance.*,emailaddress as emailaddress_before,'<NEW_PD_VALUE>' as emailaddress_after from notificationinstance where emailaddress = '<CURRENT_PD_VALUE>' ;

-- + UPDATE (be careful)
update notificationinstance set emailaddress = '<NEW_PD_VALUE>' where emailaddress = '<CURRENT_PD_VALUE>' ;

-- Type        : update
-- Origin      : jira
-- Description : notification scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditNotificationScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Notification schemes'
--     5. Click 'Edit' under specific notification scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/notificationscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select notificationscheme.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from notificationscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update notificationscheme set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : notification scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditNotificationScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Notification schemes'
--     5. Click 'Edit' under specific notification scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/notificationscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select notificationscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from notificationscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update notificationscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : permission scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPermissionScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Permission schemes'
--     5. Click 'Edit' under specific permission scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/permissionscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select permissionscheme.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from permissionscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update permissionscheme set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : permission scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPermissionScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Permission schemes'
--     5. Click 'Edit' under specific permission scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/permissionscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select permissionscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from permissionscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update permissionscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : dashboard
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select portalpage.*,description as description_before,replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as description_after from portalpage where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update portalpage set description = replace(description, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : dashboard
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select portalpage.*,pagename as pagename_before,replace(pagename, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as pagename_after from portalpage where pagename like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update portalpage set pagename = replace(pagename, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where pagename like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : priority
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select priority.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from priority where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update priority set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : priority
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select priority.*,iconurl as iconurl_before,'<NEW_PD_VALUE>' as iconurl_after from priority where iconurl like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update priority set iconurl = '<NEW_PD_VALUE>' where iconurl like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : priority
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select priority.*,pname as pname_before,replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as pname_after from priority where pname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update priority set pname = replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select project.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from project where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update project set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
-- 
-- SQL update of original project key is not supported because it could break JIRA
-- In order to alter original project key:
-- 	create new project
-- 	move all issues to new project
-- 	remove old project
select project.*,originalkey as originalkey_before,replace(originalkey, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as originalkey_after from project where originalkey like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
-- SQL update of original project key is not supported because it could break JIRA
-- In order to alter original project key:
-- 	create new project
-- 	move all issues to new project
-- 	remove old project
 
 
-- Type        : update
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
-- 
-- SQL update of project key is not supported because it could break JIRA
-- Please follow steps described above
select project.*,pkey as pkey_before,replace(pkey, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as pkey_after from project where pkey like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
-- SQL update of project key is not supported because it could break JIRA
-- Please follow steps described above
 
 
-- Type        : update
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select project.*,pname as pname_before,replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as pname_after from project where pname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update project set pname = replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select project.*,url as url_before,'<NEW_PD_VALUE>' as url_after from project where url like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update project set url = '<NEW_PD_VALUE>' where url like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : project category
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/projectcategories/ViewProjectCategories!default.jspa
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. 'Project categories' sidebar
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/projectCategory
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select projectcategory.*,cname as cname_before,replace(cname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as cname_after from projectcategory where cname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update projectcategory set cname = replace(cname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where cname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : project category
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/projectcategories/ViewProjectCategories!default.jspa
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. 'Project categories' sidebar
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/projectCategory
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select projectcategory.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from projectcategory where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update projectcategory set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : version
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select projectversion.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from projectversion where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update projectversion set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : version
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select projectversion.*,url as url_before,'<NEW_PD_VALUE>' as url_after from projectversion where url like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update projectversion set url = '<NEW_PD_VALUE>' where url like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : version
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select projectversion.*,vname as vname_before,replace(vname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as vname_after from projectversion where vname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update projectversion set vname = replace(vname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where vname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : entity property value
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select propertystring.*,propertyvalue as propertyvalue_before,cast(replace(cast(propertyvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as propertyvalue_after from propertystring where propertyvalue like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update propertystring set propertyvalue = cast(replace(cast(propertyvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where propertyvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : entity property value
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select propertytext.*,propertyvalue as propertyvalue_before,cast(replace(cast(propertyvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as propertyvalue_after from propertytext where propertyvalue like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update propertytext set propertyvalue = cast(replace(cast(propertyvalue as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where propertyvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,icontitle as icontitle_before,cast(replace(cast(icontitle as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as icontitle_after from remotelink where icontitle like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set icontitle = cast(replace(cast(icontitle as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where icontitle like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,relationship as relationship_before,replace(relationship, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as relationship_after from remotelink where relationship like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set relationship = replace(relationship, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where relationship like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,statuscategorykey as statuscategorykey_before,replace(statuscategorykey, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as statuscategorykey_after from remotelink where statuscategorykey like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set statuscategorykey = replace(statuscategorykey, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where statuscategorykey like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,statusdescription as statusdescription_before,cast(replace(cast(statusdescription as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as statusdescription_after from remotelink where statusdescription like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set statusdescription = cast(replace(cast(statusdescription as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where statusdescription like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,statusiconlink as statusiconlink_before,'<NEW_PD_VALUE>' as statusiconlink_after from remotelink where statusiconlink like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set statusiconlink = '<NEW_PD_VALUE>' where statusiconlink like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,statusicontitle as statusicontitle_before,cast(replace(cast(statusicontitle as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as statusicontitle_after from remotelink where statusicontitle like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set statusicontitle = cast(replace(cast(statusicontitle as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where statusicontitle like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,statusiconurl as statusiconurl_before,'<NEW_PD_VALUE>' as statusiconurl_after from remotelink where statusiconurl like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set statusiconurl = '<NEW_PD_VALUE>' where statusiconurl like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,statusname as statusname_before,replace(statusname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as statusname_after from remotelink where statusname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set statusname = replace(statusname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where statusname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,summary as summary_before,cast(replace(cast(summary as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as summary_after from remotelink where summary like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set summary = cast(replace(cast(summary as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where summary like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,title as title_before,replace(title, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as title_after from remotelink where title like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set title = replace(title, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where title like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select remotelink.*,url as url_before,'<NEW_PD_VALUE>' as url_after from remotelink where url like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update remotelink set url = '<NEW_PD_VALUE>' where url like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue resolution
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select resolution.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from resolution where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update resolution set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue resolution
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select resolution.*,iconurl as iconurl_before,'<NEW_PD_VALUE>' as iconurl_after from resolution where iconurl like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update resolution set iconurl = '<NEW_PD_VALUE>' where iconurl like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue resolution
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select resolution.*,pname as pname_before,replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as pname_after from resolution where pname like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update resolution set pname = replace(pname, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue security level
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurities!default.jspa?atl_token=schemeId=${scheme}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue security schemes'
--     5. Click 'Security Levels' under a specific issue security scheme name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select schemeissuesecuritylevels.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from schemeissuesecuritylevels where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update schemeissuesecuritylevels set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : issue security level
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurities!default.jspa?atl_token=schemeId=${scheme}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue security schemes'
--     5. Click 'Security Levels' under a specific issue security scheme name
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select schemeissuesecuritylevels.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from schemeissuesecuritylevels where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update schemeissuesecuritylevels set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select searchrequest.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from searchrequest where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update searchrequest set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select searchrequest.*,filtername as filtername_before,replace(filtername, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as filtername_after from searchrequest where filtername like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs
update searchrequest set filtername = replace(filtername, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where filtername like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select searchrequest.*,filtername_lower as filtername_lower_before,replace(filtername_lower, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as filtername_lower_after from searchrequest where filtername_lower like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update searchrequest set filtername_lower = replace(filtername_lower, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where filtername_lower like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select searchrequest.*,reqcontent as reqcontent_before,cast(replace(cast(reqcontent as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as reqcontent_after from searchrequest where reqcontent like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update searchrequest set reqcontent = cast(replace(cast(reqcontent as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where reqcontent like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : recorded last operations for recently used entities (users, projects, issues etc.)
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select userhistoryitem.*,data as data_before,cast(replace(cast(data as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as data_after from userhistoryitem where data like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update userhistoryitem set data = cast(replace(cast(data as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where data like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select workflowscheme.*,description as description_before,cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as description_after from workflowscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update workflowscheme set description = cast(replace(cast(description as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select workflowscheme.*,name as name_before,replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as name_after from workflowscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update workflowscheme set name = replace(name, '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : update
-- Origin      : jira
-- Description : worklog
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Work Log' tab
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select worklog.*,worklogbody as worklogbody_before,cast(replace(cast(worklogbody as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as worklogbody_after from worklog where worklogbody like '%<CURRENT_PD_VALUE>%' ;

-- + UPDATE (be careful)
update worklog set worklogbody = cast(replace(cast(worklogbody as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) where worklogbody like '%<CURRENT_PD_VALUE>%' ;

